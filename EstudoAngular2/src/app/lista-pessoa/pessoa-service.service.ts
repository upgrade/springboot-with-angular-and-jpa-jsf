import { Injectable } from '@angular/core';

@Injectable()
export class PessoaServiceService {

  nomesPessoas: string[] = ['Joao', 'Maria', 'Anacxvxc'];

  constructor() { }

  getPessoas() : string[]{
      // return ['João', 'Maria', 'Jose', 'Ana', 'Joaquim'];
      return this.nomesPessoas;
  }

  setPessoa(nome: string): void{
    this.nomesPessoas.push(nome);
  }

}
