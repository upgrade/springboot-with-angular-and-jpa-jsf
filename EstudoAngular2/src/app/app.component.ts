import { Component } from '@angular/core';

import { AlertaService } from './alerta.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Estudos Angular!';
  foto : string  = 'favicon.ico';
  desenvolvimento: string[] = ['angular', 'javaScript', 'TypeScript', 'HTML'];
  valor: string;

  constructor (private service : AlertaService){}
  
  enviarMsg(): void{
    this.service.msgAlerta();
  }

  // constructor() {
  //   private service = new AlertaService;
  // }

  valorPassado(valorPassado){
    this.valor = valorPassado;
  }


}
