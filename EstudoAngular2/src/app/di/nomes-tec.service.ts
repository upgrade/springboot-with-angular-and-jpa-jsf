import { Injectable } from '@angular/core';
import { MeuLogService } from './meu-log.service';

@Injectable()
export class NomesTecService {

  constructor(private meuLog: MeuLogService) {
    
   }

  getNomesTec(): string []{
    this.meuLog.SetLog('consultou o array de tecnologias');
    return ['Angular', 'Typescript', 'Javascript', 'HTML'];
  }

}
