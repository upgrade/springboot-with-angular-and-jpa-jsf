import { Injectable } from '@angular/core';
import { ContatoModelo } from '../modelos/contato-modelo';
import { EventEmitter } from '@angular/common/src/facade/async';

@Injectable()
export class ContatosDataBaseService {

  meuContatos: ContatoModelo [] = [];
  enviarContato = new EventEmitter();

  constructor() { }

  setContato(novoContato: ContatoModelo): void{
    this.meuContatos.push(novoContato);
    this.enviarContato.emit(this.meuContatos);
  }

  getContato(id: number): ContatoModelo{
    let contato: ContatoModelo;
    contato = this.meuContatos[id];
    return contato;
  }

}
