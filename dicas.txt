npm -install -g typescript -> instalar o typescript

instalar angular 2
npm install -g angular-cli

ng new nomeDoProjeto -> criar projeto

ng init -> cria tb mas sem criar a pasta, se ja existe a pasta entre nela 
e digite o comando abaixo

ng server -> build do projeto, criando o servidor e rodando a aplicação
 
ng generate ou ng g -> cria os tipos de objetos que queremos, componentes
interface, serviços
ng g [objeto] [nome do objeto] 
objetos:  componentes ng g component ou c Nome
        serviços    ng g service ou s Nome
        Classe      ng g class ou cl nome
        interface   ng g interface ou i nome

ng lint -> ajuda na organização do codigo para as boas praticas mostrando
erros de espaços, identação ou chamadas não usadas

ng test -> executar testes unitários dos componentes usando o karma 
ou jasmine. mostra no prompt erros possiveis, sucesso dentro de cada
componente

@Component({ -> decoration
  selector: 'app-lista-pessoa',                ---
  templateUrl: './lista-pessoa.component.html',--- > metadata
  styleUrls: ['./lista-pessoa.component.css']  ---
})

ng g c lista-pessoa -> criação de um componente lista-pessoa com o template e arquivos necessarios

data binding -> enviar ou sincronizar dados entre classe do componente e o template

<input type="text" [(ngModel)] = "nome"> -> muda o que tem em alguma interpolação
<p> {{nome}} </p>

diretivas estruturadas -> modificam a estrutura da pagina como inserindo um ngFor ngIf, executar
como uma estrutura de repetição quando altera a pagina conforme seu tamanho

diretivas de atributos -> altera a aparecncia, comportamento ou conteudo do elemento
que esta na tela como ngClass ou ngStyle, ngModel, não modifica a estrutura mas sim modifica
os elementos

ng d s nome-service -> para os serviços por exemplo adicionar string em um vetor

<input type="text" [ngModel]="nome" (ngModelChange)="mudar($event)">
<p>{{nome}}</p> -> recebe de forma dinamica o valor digitado no input

ngif -> condicional
ngSwitch -> para várias validações
<div [ngSwitch]="expressão condicional">
<p *ngSwitchCase="condição 1">mostra conteúdo 1</p>
<p *ngSwitchCase="condição 2">mostra conteúdo 2</p>
<p *ngSwitchCase="condição 3">mostra conteúdo 3</p>
<p *ngSwitchCase="condição 4">mostra conteúdo 4</p>
<p *ngSwitchDefault>mostra conteúdo padrão</p>
</div>

ngFor -> laço de repetição
<li *ngFor="let variável_local of array ">
<li *ngFor="let variável_local of array, let i = index"> -> caso precise da index

ngClass -> manipulação das classes css dentro do projeto

ngStyle -> configurar atributos separados dentro do html

(keyup) -> evento que executa cada tecla q apertamos
(keyup.enter) -> evento ao pressionar a tecla enter
blur -> evento quando o usuario tira o foco de dentro de uma caixa de texto

instalando bootstrap
digitar no console npm install bootstrap@next --save